use clap::{command, Arg, Command};
use directories::ProjectDirs;
use log::error;
use marne::{run, run_args, send, setup_args, Config, Fetcher, MatrixFetcher};

#[tokio::main]
async fn main() {
    env_logger::init();
    let paths = ProjectDirs::from("is", "timorl", "marne").unwrap();
    let mut setup_command = Command::new("setup").about("set up the login session");
    for arg in setup_args() {
        setup_command = setup_command.arg(arg);
    }
    let mut run_command = Command::new("run").about("run the notifier");
    for arg in run_args() {
        run_command = run_command.arg(arg);
    }
    let silence_command = Command::new("stfu")
        .about("silence thine frantic utterances")
        .arg(
            Arg::new("silence-minutes")
                .index(1)
                .help("Duration of the silence, in minutes"),
        );
    let get_rooms_command = Command::new("get").about("get a list of known rooms");
    let mute_room_command = Command::new("mute").about("mute a room").arg(
        Arg::new("local-room-id")
            .index(1)
            .help("The index of the room, as displayed in the get command"),
    );
    let unmute_room_command = Command::new("unmute").about("unmute a room").arg(
        Arg::new("local-room-id")
            .index(1)
            .help("The index of the room, as displayed in the get command"),
    );
    let rooms_command = Command::new("rooms")
        .about("manipulate room muting and info")
        .subcommand(get_rooms_command)
        .subcommand(mute_room_command)
        .subcommand(unmute_room_command);
    let exit_command = Command::new("die").about("kill the notifier");
    let options = command!()
        .subcommand(setup_command)
        .subcommand(run_command)
        .subcommand(silence_command)
        .subcommand(rooms_command)
        .subcommand(exit_command)
        .get_matches();

    let data_dir = paths.data_dir().to_path_buf();
    let runtime_dir = paths.runtime_dir().unwrap().to_path_buf();

    let common_args = Config {
        data_dir,
        runtime_dir,
    };

    match options.subcommand() {
        Some(("setup", options)) => {
            match MatrixFetcher::setup(common_args, options).await {
                Err(e) => error!("Setting up a session failed: {}.", e),
                Ok(()) => println!("Session set up successfully, use the `run` subcommand to actually run the program."),
            }
        },
        Some(("run", options)) => {
            if let Err(e) = run(common_args, options).await {
                error!("Failed to run: {}", e);
            }
        },
        Some(("stfu", options)) => {
            if let Err(e) = send::silence(common_args, options).await {
                    error!("Failed to send silence: {}", e);
            }
        },
        Some(("rooms", options)) => {
            match options.subcommand() {
                Some(("get", _)) => match send::get_rooms(common_args).await {
                    Ok(rooms) => println!("{}", rooms),
                    Err(e) => error!("Failed to get rooms: {}", e),
                },
                Some(("mute", options)) => match send::mute_room(common_args, options).await {
                    Ok(rooms) => println!("{}", rooms),
                    Err(e) => error!("Failed to mute room: {}", e),
                },
                Some(("unmute", options)) => match send::unmute_room(common_args, options).await {
                    Ok(rooms) => println!("{}", rooms),
                    Err(e) => error!("Failed to mute room: {}", e),
                },
                None => {
                    panic!("Please provide a further subcommand, e.g. \"help\" is a good start.");
                },
                _ => unreachable!(),
            }
        },
        Some(("die", _)) => {
            if let Err(e) = send::exit(common_args).await {
                    error!("Failed to send exit: {}", e);
            }
        },
        None => {
            panic!("Please provide a subcommand, e.g. \"help\" is a good start.");
        },
        _ => unreachable!(),
    }
}
