use crate::{ArgList, Config, Error};
use tokio::sync::mpsc;

pub type NotificationId = String;
pub const ALL_NOTIFICATIONS: &str = "";

#[derive(Debug)]
pub struct Notification {
    pub summary: String,
    pub body: String,
    pub command: String,
}

pub type NotificationMessage = (NotificationId, Option<Notification>);

pub type NotificationSender = mpsc::Sender<NotificationMessage>;
pub type NotificationReceiver = mpsc::Receiver<NotificationMessage>;

pub use crate::control::InternalMessage as ControlMessage;

pub type ControlSender = mpsc::Sender<ControlMessage>;
pub type ControlReceiver = mpsc::Receiver<ControlMessage>;

#[async_trait::async_trait]
pub trait Runnable {
    type Error: Into<Error>;

    async fn run(self, control: ControlReceiver) -> Result<(), Self::Error>;
}

#[async_trait::async_trait]
pub trait Fetcher<E: Into<Error>>: Sized + Runnable<Error = E> {
    fn setup_args<'a>() -> ArgList<'a>;

    async fn setup(config: Config, options: &clap::ArgMatches) -> Result<(), E>;

    fn run_args<'a>() -> ArgList<'a>;

    fn build(
        config: Config,
        options: &clap::ArgMatches,
        notifications: NotificationSender,
    ) -> Result<Self, E>;
}

#[async_trait::async_trait]
pub trait Notifier<E: Into<Error>>: Sized + Runnable<Error = E> {
    fn build(options: &clap::ArgMatches, notifications: NotificationReceiver) -> Result<Self, E>;

    fn args<'a>() -> ArgList<'a>;
}
