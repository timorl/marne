use super::{send, socket_file_name, stream, Message, Response};
use crate::{matrix::KnownRooms, Config, Error};
use std::time::Duration;
use tokio::{io::AsyncReadExt, time::timeout};

pub async fn exit(common_args: Config) -> Result<(), Error> {
    let mut writer = stream(socket_file_name(common_args.runtime_dir)).await?;
    send(&mut writer, Message::Exit).await
}

pub async fn silence(common_args: Config, options: &clap::ArgMatches) -> Result<(), Error> {
    let mut writer = stream(socket_file_name(common_args.runtime_dir)).await?;
    let duration = Duration::from_secs(options.value_of_t_or_exit::<u64>("silence-minutes") * 60);
    send(&mut writer, Message::Silence(duration)).await
}

pub async fn get_rooms(common_args: Config) -> Result<KnownRooms, Error> {
    let socket_file_name = socket_file_name(common_args.runtime_dir);
    let mut stream = stream(socket_file_name).await?;
    send(&mut stream, Message::GetRooms).await?;
    let mut msg = String::new();
    timeout(Duration::from_secs(30), stream.read_to_string(&mut msg))
        .await
        .map_err(|_| Error::InternalError("timed out waiting for response".to_string()))??;
    match Response::decode(msg)? {
        Response::KnownRooms(rooms) => rooms.map_err(Error::InternalError),
    }
}

pub async fn mute_room(
    common_args: Config,
    options: &clap::ArgMatches,
) -> Result<KnownRooms, Error> {
    let local_room_id = options.value_of_t_or_exit::<usize>("local-room-id");
    let socket_file_name = socket_file_name(common_args.runtime_dir);
    let mut stream = stream(socket_file_name).await?;
    send(&mut stream, Message::MuteRoom(local_room_id)).await?;
    let mut msg = String::new();
    timeout(Duration::from_secs(30), stream.read_to_string(&mut msg))
        .await
        .map_err(|_| Error::InternalError("timed out waiting for response".to_string()))??;
    match Response::decode(msg)? {
        Response::KnownRooms(rooms) => rooms.map_err(Error::InternalError),
    }
}

pub async fn unmute_room(
    common_args: Config,
    options: &clap::ArgMatches,
) -> Result<KnownRooms, Error> {
    let local_room_id = options.value_of_t_or_exit::<usize>("local-room-id");
    let socket_file_name = socket_file_name(common_args.runtime_dir);
    let mut stream = stream(socket_file_name).await?;
    send(&mut stream, Message::UnmuteRoom(local_room_id)).await?;
    let mut msg = String::new();
    timeout(Duration::from_secs(30), stream.read_to_string(&mut msg))
        .await
        .map_err(|_| Error::InternalError("timed out waiting for response".to_string()))??;
    match Response::decode(msg)? {
        Response::KnownRooms(rooms) => rooms.map_err(Error::InternalError),
    }
}
