use super::{InternalMessage, Message, Response};
use crate::{ControlSender, Error};
use tokio::{
    io::{AsyncRead, AsyncReadExt},
    sync::{mpsc, oneshot},
    time::{timeout, Duration},
};

pub struct Handler {
    commands_for_fetcher: ControlSender,
    _commands_for_notifier: ControlSender,
    stop: mpsc::Sender<()>,
}

impl Handler {
    pub fn new(
        commands_for_fetcher: ControlSender,
        _commands_for_notifier: ControlSender,
        stop: mpsc::Sender<()>,
    ) -> Self {
        Handler {
            commands_for_fetcher,
            _commands_for_notifier,
            stop,
        }
    }

    pub async fn handle_incoming<R: AsyncRead + Unpin>(
        &self,
        reader: &mut R,
    ) -> Result<Option<Response>, Error> {
        let mut msg = String::new();
        timeout(Duration::from_millis(500), reader.read_to_string(&mut msg))
            .await
            .map_err(|_| Error::InternalError("timed out reading from socket".to_string()))??;
        match Message::decode(msg)? {
            Message::Exit => self
                .stop
                .send(())
                .await
                .map_err(|_| Error::InternalError("failed to send exit signal".to_string()))
                .map(|_| None),
            Message::Silence(duration) => self
                .commands_for_fetcher
                .send(InternalMessage::Silence(duration))
                .await
                .map_err(|_| Error::InternalError("failed to send silence command".to_string()))
                .map(|_| None),
            Message::GetRooms => {
                let (result_for_us, result_from_fetcher) = oneshot::channel();
                self.commands_for_fetcher
                    .send(InternalMessage::GetRooms(result_for_us))
                    .await
                    .map_err(|_| {
                        Error::InternalError("failed to send get rooms command".to_string())
                    })?;
                result_from_fetcher
                    .await
                    .map_err(|_| {
                        Error::InternalError(
                            "fetcher failed to send get rooms response".to_string(),
                        )
                    })
                    .map(Ok)
                    .map(Response::KnownRooms)
                    .map(Some)
            }
            Message::MuteRoom(local_room_id) => {
                let (result_for_us, result_from_fetcher) = oneshot::channel();
                self.commands_for_fetcher
                    .send(InternalMessage::MuteRoom(local_room_id, result_for_us))
                    .await
                    .map_err(|_| {
                        Error::InternalError("failed to send mute room command".to_string())
                    })?;
                result_from_fetcher
                    .await
                    .map_err(|_| {
                        Error::InternalError(
                            "fetcher failed to send mute room response".to_string(),
                        )
                    })
                    .map(Response::KnownRooms)
                    .map(Some)
            }
            Message::UnmuteRoom(local_room_id) => {
                let (result_for_us, result_from_fetcher) = oneshot::channel();
                self.commands_for_fetcher
                    .send(InternalMessage::UnmuteRoom(local_room_id, result_for_us))
                    .await
                    .map_err(|_| {
                        Error::InternalError("failed to send mute room command".to_string())
                    })?;
                result_from_fetcher
                    .await
                    .map_err(|_| {
                        Error::InternalError(
                            "fetcher failed to send mute room response".to_string(),
                        )
                    })
                    .map(Response::KnownRooms)
                    .map(Some)
            }
        }
    }
}
