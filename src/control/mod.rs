use crate::{matrix::KnownRooms, Error};
use serde::{Deserialize, Serialize};
use std::{path::PathBuf, time::Duration};
use tokio::{
    io::{AsyncRead, AsyncWrite, AsyncWriteExt},
    net::{UnixListener, UnixStream},
    sync::oneshot,
};

mod handler;
mod manager;
pub mod send;

use handler::Handler;
pub use manager::{run, run_args, setup_args};

#[derive(Serialize, Deserialize, Debug)]
pub enum Message {
    Exit,
    Silence(Duration),
    GetRooms,
    MuteRoom(usize),
    UnmuteRoom(usize),
}

#[derive(Serialize, Deserialize, Debug)]
pub enum Response {
    KnownRooms(Result<KnownRooms, String>),
}

impl Message {
    fn decode(msg: String) -> Result<Self, Error> {
        Ok(serde_json::from_str(&msg)?)
    }
}

impl Response {
    fn decode(msg: String) -> Result<Self, Error> {
        Ok(serde_json::from_str(&msg)?)
    }
}

#[derive(Debug)]
pub enum InternalMessage {
    Exit,
    Silence(Duration),
    GetRooms(oneshot::Sender<KnownRooms>),
    MuteRoom(usize, oneshot::Sender<Result<KnownRooms, String>>),
    UnmuteRoom(usize, oneshot::Sender<Result<KnownRooms, String>>),
}

fn socket_file_name(mut runtime_dir: PathBuf) -> PathBuf {
    runtime_dir.push("control.sock");
    runtime_dir
}

fn socket(socket_file_name: PathBuf) -> Result<UnixListener, Error> {
    Ok(UnixListener::bind(socket_file_name)?)
}

async fn stream(socket_file_name: PathBuf) -> Result<impl AsyncWrite + AsyncRead, Error> {
    Ok(UnixStream::connect(socket_file_name).await?)
}

async fn send<W: AsyncWrite + Unpin, M: Serialize>(
    writer: &mut W,
    message: M,
) -> Result<(), Error> {
    writer
        .write(serde_json::to_string(&message)?.as_bytes())
        .await?;
    writer.flush().await?;
    writer.shutdown().await?;
    Ok(())
}
