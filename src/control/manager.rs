use super::{send, socket, socket_file_name, Handler, Response};
use crate::{
    stop_all, ArgList, Config, Controlable, Error, Fetcher, MatrixFetcher, Notifier,
    NotifierImplementation,
};
use clap::ArgMatches;
use log::{debug, error, info, trace, warn};
use std::fs::{create_dir_all, remove_file};
use tokio::{io::AsyncWrite, net::UnixListener, sync::mpsc};

pub fn setup_args<'a>() -> ArgList<'a> {
    MatrixFetcher::setup_args()
}

pub fn run_args<'a>() -> ArgList<'a> {
    let mut result = MatrixFetcher::run_args();
    result.append(&mut NotifierImplementation::args());
    result
}

async fn handle_incoming(
    socket: &mut UnixListener,
    handler: &mut Handler,
) -> Result<Option<(Response, impl AsyncWrite)>, Error> {
    let (mut stream, _) = socket.accept().await?;
    handler
        .handle_incoming(&mut stream)
        .await
        .map(|response| response.map(|response| (response, stream)))
}

async fn work(
    mut fetcher: Controlable,
    mut notifier: Controlable,
    mut socket: UnixListener,
    mut handler: Handler,
    mut exit: mpsc::Receiver<()>,
) -> Result<(), Error> {
    loop {
        tokio::select! {
            result = fetcher.stopped() => {
                if let Err(e) = result {
                    error!(target: "marne", "Fetcher stopped prematurely: {}", e);
                }
                return stop_all([notifier]).await;
            },
            result = notifier.stopped() => {
                if let Err(e) = result {
                    error!(target: "marne", "Notifier stopped prematurely: {}", e);
                }
                return stop_all([fetcher]).await;
            },
            result = handle_incoming(&mut socket, &mut handler) => match result {
                Ok(Some((response, mut stream))) => match send(&mut stream, response).await {
                    Ok(()) => debug!(target: "marne", "Responded to query."),
                    Err(e) => warn!(target: "marne", "Failed sending response: {}", e),
                },
                Ok(None) => trace!(target: "marne", "Handled stream, no response required."),
                Err(e) => warn!(target: "marne", "Failed handling socket stream: {}", e),
            },
            _ = exit.recv() => {
                debug!(target: "marne", "Stopping the manager.");
                return stop_all([notifier, fetcher]).await;
            }
        }
    }
}

pub async fn run(common_args: Config, options: &ArgMatches) -> Result<(), Error> {
    create_dir_all(&common_args.runtime_dir)?;
    let (notifications_for_notifier, notifications_from_fetcher) = mpsc::channel(100);
    let socket_file_name = socket_file_name(common_args.runtime_dir.clone());
    let socket = socket(socket_file_name.clone())?;
    let notifier: Controlable =
        NotifierImplementation::build(options, notifications_from_fetcher)?.into();
    let commands_for_notifier = notifier.control_channel();
    let fetcher: Controlable =
        MatrixFetcher::build(common_args, options, notifications_for_notifier)?.into();
    let commands_for_fetcher = fetcher.control_channel();
    let (stop, exit) = mpsc::channel(1);
    let handler = Handler::new(commands_for_fetcher, commands_for_notifier, stop);
    info!(target: "marne", "Starting the manager.");
    let result = work(fetcher, notifier, socket, handler, exit).await;
    info!(target: "marne", "Manager stopped.");
    remove_file(socket_file_name)?;
    result
}
