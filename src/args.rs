use clap;
use std::path;

pub type ArgList<'help> = Vec<clap::Arg<'help>>;

pub struct CommonArgs {
    pub data_dir: path::PathBuf,
}
