use crate::{
    ArgList, ControlMessage, ControlReceiver, Error, Notification, NotificationId,
    NotificationReceiver, Notifier, Runnable,
};
use log::{debug, error};
use sxmotify::Namespace as SxmoNamespace;

pub struct SxmoNotifier {
    notifications_from_world: NotificationReceiver,
    namespace: SxmoNamespace,
}

impl SxmoNotifier {
    fn set_notification(&mut self, id: NotificationId, message: Notification) {
        if let Err(e) = self.namespace.set_notification(
            id,
            format!("{}: {}", message.summary, message.body),
            message.command,
            None,
        ) {
            error!(target: "marne-sxmo", "Setting notification failed: {}", e);
        }
    }

    fn clear_notification(&mut self, id: NotificationId) {
        if let Err(e) = self.namespace.clear_notification(id) {
            error!(target: "marne-sxmo", "Clearing notification failed: {}", e)
        }
    }

    fn handle_notification(&mut self, id: NotificationId, maybe_message: Option<Notification>) {
        let id = hex::encode(id);
        match maybe_message {
            Some(message) => {
                self.set_notification(id, message);
            }
            None => {
                self.clear_notification(id);
            }
        }
    }
}

#[async_trait::async_trait]
impl Runnable for SxmoNotifier {
    type Error = Error;

    async fn run(mut self, mut control: ControlReceiver) -> Result<(), Self::Error> {
        loop {
            tokio::select! {
                maybe_notification = self.notifications_from_world.recv() => match maybe_notification {
                    Some((id, maybe_message)) => self.handle_notification(id, maybe_message),
                    None => return Err(Error::InternalError("notification stream closed unexpectedly".to_string()))
                },
                command = control.recv() => match command {
                    None => return Err(Error::InternalError("command stream closed unexpectedly".to_string())),
                    Some(ControlMessage::Exit) => break,
                    _ => debug!(target: "marne-sxmo", "Received unexpected control command: {:?}", command),
                },
            }
        }
        let SxmoNotifier { namespace, .. } = self;
        Ok(namespace.cleanup()?)
    }
}

#[async_trait::async_trait]
impl Notifier<Error> for SxmoNotifier {
    fn build(
        options: &clap::ArgMatches,
        notifications_from_world: NotificationReceiver,
    ) -> Result<Self, Self::Error> {
        Ok(SxmoNotifier {
            notifications_from_world,
            namespace: SxmoNamespace::new(options.value_of_t_or_exit("notification-namespace")),
        })
    }

    fn args<'a>() -> ArgList<'a> {
        vec![clap::Arg::new("notification-namespace")
            .long("notification-namespace")
            .help("Sets the namespace of the notifications created by this instance")
            .takes_value(true)
            .required(true)]
    }
}
