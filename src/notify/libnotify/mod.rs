use crate::{
    ArgList, ControlMessage, ControlReceiver, Error, Notification, NotificationId,
    NotificationReceiver, Notifier, Runnable,
};
use log::debug;
use notify_rust::{Notification as Notify, NotificationHandle};
use std::collections::HashMap;

pub struct StandardNotifier {
    notifications_from_world: NotificationReceiver,
    active_notifications: HashMap<NotificationId, NotificationHandle>,
}

impl StandardNotifier {
    fn new_notification(message: Notification) -> NotificationHandle {
        Notify::new()
            .summary(&message.summary)
            .body(&message.body)
            .show()
            .expect("Showing notifications should work.")
    }

    fn set_notification(&mut self, id: NotificationId, message: Notification) {
        let notification = match self.active_notifications.remove(&id) {
            Some(mut notification) => {
                notification.summary(&message.summary).body(&message.body);
                notification.update();
                notification
            }
            None => Self::new_notification(message),
        };
        self.active_notifications.insert(id, notification);
    }

    fn clear_notification(&mut self, id: NotificationId) {
        match self.active_notifications.remove(&id) {
            Some(notification) => notification.close(),
            None => debug!(target: "marne-libnotify", "Attempted to clear unknown notification."),
        }
    }

    fn handle_notification(&mut self, id: NotificationId, maybe_message: Option<Notification>) {
        match maybe_message {
            Some(message) => {
                self.set_notification(id, message);
            }
            None => {
                self.clear_notification(id);
            }
        }
    }
}

#[async_trait::async_trait]
impl Runnable for StandardNotifier {
    type Error = Error;

    async fn run(mut self, mut control: ControlReceiver) -> Result<(), Self::Error> {
        loop {
            tokio::select! {
                maybe_notification = self.notifications_from_world.recv() => match maybe_notification {
                    Some((id, maybe_message)) => self.handle_notification(id, maybe_message),
                    None => return Err(Error::InternalError("notification stream closed unexpectedly".to_string()))
                },
                command = control.recv() => match command {
                    None => return Err(Error::InternalError("command stream closed unexpectedly".to_string())),
                    Some(ControlMessage::Exit) => break,
                    _ => debug!(target: "marne-libnotify", "Received unexpected control command: {:?}", command),
                },
            }
        }
        Ok(())
    }
}

#[async_trait::async_trait]
impl Notifier<Error> for StandardNotifier {
    fn build(
        _options: &clap::ArgMatches,
        notifications_from_world: NotificationReceiver,
    ) -> Result<Self, Self::Error> {
        Ok(StandardNotifier {
            notifications_from_world,
            active_notifications: HashMap::new(),
        })
    }

    fn args<'a>() -> ArgList<'a> {
        Vec::new()
    }
}
