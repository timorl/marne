mod config;
mod control;
mod error;
mod matrix;
mod notify;
mod task;
mod traits;

#[cfg(all(feature = "libnotify", feature = "sxmo"))]
compile_error!(
    "Don't be silly, you cannot have the same thing working on both sxmo and other systems."
);

pub use config::Config;
pub use control::{run, run_args, send, setup_args};
pub use matrix::MatrixFetcher;
pub use traits::*;

use const_format::formatcp;

use config::ArgList;
use error::Error;
use task::{stop_all, Controlable};

#[cfg(feature = "libnotify")]
use notify::StandardNotifier as NotifierImplementation;
#[cfg(feature = "sxmo")]
use notify::SxmoNotifier as NotifierImplementation;

pub const APP_ID: &str = formatcp!("MaRNE {}", clap::crate_version!());
