use matrix_sdk::{
    ruma::identifiers::Error as IdentifierError, ClientBuildError, Error as SdkError, StoreError,
};
use serde_json::Error as SerdeError;
use std::io::Error as IoError;
use thiserror::Error;
use url::ParseError;

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    SdkError(#[from] SdkError),
    #[error(transparent)]
    IdentifierError(#[from] IdentifierError),
    #[error(transparent)]
    ClientBuildError(#[from] ClientBuildError),
    #[error(transparent)]
    SerdeError(#[from] SerdeError),
    #[error(transparent)]
    IoError(#[from] IoError),
    #[error(transparent)]
    UrlError(#[from] ParseError),
    #[error("Internal error: {0}")]
    InternalError(String),
}

impl From<StoreError> for Error {
    fn from(e: StoreError) -> Self {
        SdkError::from(e).into()
    }
}
