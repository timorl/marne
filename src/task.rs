use crate::{ControlMessage, ControlSender, Error, Runnable};
use futures::FutureExt;
use std::future::Future;
use std::{boxed::Box, pin::Pin};
use tokio::sync::mpsc;

pub struct Controlable {
    control_for_task: ControlSender,
    task: Pin<Box<dyn Future<Output = Result<(), Error>>>>,
}

impl Controlable {
    pub fn control_channel(&self) -> ControlSender {
        self.control_for_task.clone()
    }

    pub async fn stopped(&mut self) -> Result<(), Error> {
        (&mut self.task).await
    }

    pub async fn stop(self) -> Result<(), Error> {
        if self
            .control_for_task
            .send(ControlMessage::Exit)
            .await
            .is_ok()
        {
            self.task.await?;
        }
        Ok(())
    }
}

pub async fn stop_all<Tasks: IntoIterator<Item = Controlable>>(tasks: Tasks) -> Result<(), Error> {
    for task in tasks {
        task.stop().await?;
    }
    Ok(())
}

impl<E: Into<Error> + 'static, R: Runnable<Error = E> + 'static> From<R> for Controlable
where
    Error: From<E>,
{
    fn from(runnable: R) -> Self {
        let (control_for_task, control_from_outside) = mpsc::channel(10);
        Controlable {
            control_for_task,
            task: Box::pin(runnable.run(control_from_outside).map(|r| Ok(r?))),
        }
    }
}
