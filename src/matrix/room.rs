use log::warn;
use matrix_sdk::room::Room;

pub fn joined<I: IntoIterator<Item = Room>>(rooms: I) -> impl Iterator<Item = Room> {
    rooms
        .into_iter()
        .filter(|room| matches!(room, Room::Joined(_)))
}

pub async fn name(room: &Room) -> String {
    match room.display_name().await {
        Ok(name) => name,
        Err(e) => {
            warn!(target: "matrix-fetcher", "Failed to get proper room name: {}", e);
            "".into()
        }
    }
}
