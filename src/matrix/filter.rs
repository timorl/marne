use crate::matrix::{joined_rooms, room_name};
use matrix_sdk::room::Room;
use serde::{Deserialize, Serialize};
use std::{
    collections::HashSet,
    fmt::{Display, Formatter, Result as FmtResult},
};

#[derive(Serialize, Deserialize, Debug)]
struct KnownRoom {
    room_id: String,
    room_name: String,
    muted: bool,
}

impl Display for KnownRoom {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        let m = match self.muted {
            true => "M",
            false => " ",
        };
        write!(f, "{} {}\t{}", m, self.room_id, self.room_name)
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct KnownRooms {
    known_rooms: Vec<KnownRoom>,
}

impl Display for KnownRooms {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        for (id, known_room) in self.known_rooms.iter().enumerate() {
            write!(f, "{:>3} {}\n", id, known_room)?;
        }
        Ok(())
    }
}

pub struct Filter {
    joined_rooms: Vec<(String, String)>,
    muted_rooms: HashSet<String>,
}

impl Filter {
    pub fn new(muted_rooms: HashSet<String>) -> Self {
        Filter {
            joined_rooms: Vec::new(),
            muted_rooms,
        }
    }

    pub fn filter_rooms<I: IntoIterator<Item = Room>>(&self, rooms: I) -> Vec<Room> {
        joined_rooms(rooms)
            .filter(|room| !self.muted_rooms.contains(room.room_id().as_ref()))
            .collect()
    }

    pub async fn update_rooms<I: IntoIterator<Item = Room>>(&mut self, connected_rooms: I) {
        let known_rooms: HashSet<_> = self
            .joined_rooms
            .iter()
            .map(|(room_id, _)| room_id)
            .cloned()
            .collect();
        for room in joined_rooms(connected_rooms) {
            let room_id = room.room_id();
            if !known_rooms.contains(room_id.as_ref()) {
                self.joined_rooms
                    .push((room_id.to_string(), room_name(&room).await));
            }
        }
    }

    pub fn known_rooms(&self) -> KnownRooms {
        KnownRooms {
            known_rooms: self
                .joined_rooms
                .iter()
                .cloned()
                .map(|(room_id, room_name)| KnownRoom {
                    muted: self.muted_rooms.contains(&room_id),
                    room_id,
                    room_name,
                })
                .collect(),
        }
    }

    pub fn mute_room(&mut self, local_room_id: usize) -> Result<(), String> {
        match self.joined_rooms.get(local_room_id) {
            Some((room_id, _)) => {
                self.muted_rooms.insert(room_id.clone());
                Ok(())
            }
            None => Err("Attempted to mute room out of range.".to_string()),
        }
    }

    pub fn unmute_room(&mut self, local_room_id: usize) -> Result<(), String> {
        match self.joined_rooms.get(local_room_id) {
            Some((room_id, _)) => {
                self.muted_rooms.remove(room_id);
                Ok(())
            }
            None => Err("Attempted to unmute room out of range.".to_string()),
        }
    }

    pub fn muted_rooms(&self) -> HashSet<String> {
        self.muted_rooms.clone()
    }
}
