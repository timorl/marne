use serde::{de::Error as DeserializationError, Deserialize, Deserializer, Serialize, Serializer};
use std::{collections::HashSet, fs, path};
use tokio::time;
use url::Url;

use crate::{ArgList, Error};

fn serialize_url<S: Serializer>(maybe_url: &Option<Url>, serializer: S) -> Result<S::Ok, S::Error> {
    let maybe_url = maybe_url.clone().map(|url| url.to_string());
    maybe_url.serialize(serializer)
}

fn deserialize_url<'de, D: Deserializer<'de>>(deserializer: D) -> Result<Option<Url>, D::Error> {
    let maybe_url = Option::<String>::deserialize(deserializer)?;
    let maybe_url = match maybe_url {
        Some(maybe_url) => match Url::parse(&maybe_url) {
            Ok(url) => Some(url),
            Err(e) => return Err(D::Error::custom(format!("Url parsing failed: {}", e))),
        },
        None => None,
    };
    Ok(maybe_url)
}

#[derive(Serialize, Deserialize, Default, Clone)]
pub struct FetcherOptions {
    #[serde(serialize_with = "serialize_url", deserialize_with = "deserialize_url")]
    pub homeserver: Option<Url>,
    pub sleep_duration: Option<time::Duration>,
    pub muted_rooms: HashSet<String>,
}

impl FetcherOptions {
    fn option_file_name(mut user_data_dir: path::PathBuf) -> path::PathBuf {
        user_data_dir.push("options.json");
        user_data_dir
    }

    pub fn clap_args<'a>() -> ArgList<'a> {
        vec![
            clap::Arg::new("homeserver")
                .long("homeserver")
                .short('H')
                .help("Explicit homeserver URL for servers with misconfigured `.well-known`.")
                .takes_value(true),
            clap::Arg::new("sleep-minutes")
                .long("sleep-minutes")
                .short('s')
                .help("The duration of sleep between synchronizations.")
                .takes_value(true)
                .default_value("0"),
        ]
    }

    pub fn load(user_data_dir: path::PathBuf) -> Result<Self, Error> {
        Ok(serde_json::from_str(&fs::read_to_string(
            Self::option_file_name(user_data_dir),
        )?)?)
    }

    pub fn updated_with(mut self, options: &clap::ArgMatches) -> Result<Self, Error> {
        if options.is_present("homeserver") {
            self.homeserver = Some(Url::parse(options.value_of("homeserver").unwrap())?);
        };
        if options.occurrences_of("sleep-minutes") > 0 {
            let sleep_minutes: u64 = options.value_of_t_or_exit("sleep-minutes");
            self.sleep_duration = Some(time::Duration::from_secs(sleep_minutes * 60));
        };
        Ok(self)
    }

    pub fn save(&self, user_data_dir: path::PathBuf) -> Result<(), Error> {
        fs::write(
            Self::option_file_name(user_data_dir),
            serde_json::to_string(self)?,
        )?;
        Ok(())
    }
}
