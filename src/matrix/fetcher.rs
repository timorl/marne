use crate::{
    matrix::{messages, options::FetcherOptions, room_name, sync::sync, Filter},
    ArgList, Config, ControlMessage, ControlReceiver, Error, Fetcher, NotificationSender, Runnable,
    APP_ID,
};
use log::{error, info, trace, warn};
use matrix_sdk::{
    config::RequestConfig,
    room::Room,
    ruma::{DeviceId, UserId},
    store::StateStore,
    Client, ClientBuildError, Session,
};
use rand::{distributions::Alphanumeric, thread_rng, Rng};
use rpassword::prompt_password;
use serde::{Deserialize, Serialize};
use std::{convert::TryFrom, fs, future::Future, path, pin::Pin};
use tokio::time::sleep;
use url::Url;

#[derive(Serialize, Deserialize)]
struct SessionData {
    user_data_dir: path::PathBuf,
    storage_passphrase: String,
    access_token: String,
    device_id: Box<DeviceId>,
}

pub fn user_data_dir(user_id: &UserId, mut data_dir: path::PathBuf) -> path::PathBuf {
    data_dir.push(user_id.server_name().as_str());
    data_dir.push(user_id.localpart());
    data_dir
}

pub fn session_file_name(mut user_data_dir: path::PathBuf) -> path::PathBuf {
    user_data_dir.push("session.json");
    user_data_dir
}

pub struct MatrixFetcher {
    user_id: Box<UserId>,
    user_data_dir: path::PathBuf,
    options: FetcherOptions,
    filter: Filter,
    notifications_for_world: NotificationSender,
}

impl MatrixFetcher {
    fn common_args<'a>() -> ArgList<'a> {
        let mut result = vec![clap::Arg::new("username")
            .long("username")
            .short('u')
            .help("Sets the full username in the form @user:homeserver.name")
            .takes_value(true)
            .required(true)];
        result.append(&mut FetcherOptions::clap_args());
        result
    }

    async fn build_client(
        user_id: &Box<UserId>,
        passphrase: &str,
        homeserver: Option<Url>,
        state_store: StateStore,
    ) -> Result<Client, ClientBuildError> {
        let crypto_store = state_store.open_crypto_store(Some(passphrase))?;

        let client_builder = Client::builder()
            .request_config(RequestConfig::new().retry_limit(2))
            .crypto_store(Box::new(crypto_store));

        trace!(target: "matrix-fetcher", "Setting up client.");
        Ok(match homeserver {
            Some(homeserver) => client_builder.homeserver_url(homeserver).build().await?,
            None => client_builder.user_id(user_id).build().await?,
        })
    }

    async fn login_with_password(
        user_id: Box<UserId>,
        user_data_dir: path::PathBuf,
        homeserver: Option<Url>,
    ) -> Result<SessionData, Error> {
        let storage_passphrase: String = thread_rng()
            .sample_iter(Alphanumeric)
            .take(30)
            .map(char::from)
            .collect();
        let state_store =
            StateStore::open_with_passphrase(user_data_dir.clone(), &storage_passphrase)?;
        let client =
            Self::build_client(&user_id, &storage_passphrase, homeserver, state_store).await?;

        let password = prompt_password("Password: ")?;
        trace!(target: "matrix-fetcher", "Starting login.");
        let response = client
            .login(user_id.localpart(), password.as_ref(), None, Some(APP_ID))
            .await?;
        assert_eq!(response.user_id, user_id);
        Ok(SessionData {
            user_data_dir,
            storage_passphrase,
            access_token: response.access_token,
            device_id: response.device_id,
        })
    }

    async fn setup_session(
        user_id: Box<UserId>,
        user_data_dir: path::PathBuf,
        options: FetcherOptions,
    ) -> Result<(), Error> {
        let session_data = Self::login_with_password(user_id, user_data_dir.clone(), options.homeserver.clone()).await.map_err(|e| {
            info!(target: "matrix-fetcher", "Setup failed, cleaning up...");
            if let Err(e) = fs::remove_dir_all(user_data_dir.clone()) {
                error!(target: "matrix-fetcher", "Failed cleanup, you will have to do it yourself: {}", e);
            }
            e
        })?;
        let session_file_name = session_file_name(user_data_dir);
        fs::write(session_file_name, serde_json::to_string(&session_data)?)?;
        Ok(())
    }

    fn get_old_session(&self) -> Result<SessionData, Error> {
        let session_file_name = session_file_name(self.user_data_dir.clone());
        Ok(serde_json::from_str(&fs::read_to_string(
            session_file_name,
        )?)?)
    }

    async fn login_with_session(&self) -> Result<Client, Error> {
        let session = self.get_old_session()?;
        let SessionData {
            user_data_dir,
            storage_passphrase,
            access_token,
            device_id,
        } = session;
        let state_store = StateStore::open_with_passphrase(user_data_dir, &storage_passphrase)?;
        let client = Self::build_client(
            &self.user_id,
            &storage_passphrase,
            self.options.homeserver.clone(),
            state_store,
        )
        .await?;

        client
            .restore_login(Session {
                user_id: self.user_id.clone(),
                device_id,
                access_token,
            })
            .await?;
        Ok(client)
    }

    async fn room_notification(&self, room: &Room, sync_token: String) {
        let name = room_name(room).await;
        let last_seen = match room.user_read_receipt(&self.user_id).await {
            Ok(last_seen) => last_seen,
            Err(e) => panic!("{}", e),
        }
        .map(|(event_id, _)| event_id);
        let messages = messages::messages_until(room, sync_token, last_seen).await;
        let notification = messages::create_notification(name, messages);
        if let Err(e) = self
            .notifications_for_world
            .send((room.room_id().to_string(), notification))
            .await
        {
            error!(target: "matrix-fetcher", "Send to world failed: {}", e);
        }
    }

    async fn generate_notifications(&self, client: &Client) {
        for room in self.filter.filter_rooms(client.rooms()) {
            self.room_notification(&room, client.sync_token().await.expect("We just synced."))
                .await;
        }
    }

    fn sleep_task(&self) -> Pin<Box<dyn Future<Output = ()> + Send>> {
        match self.options.sleep_duration {
            Some(duration) => Box::pin(sleep(duration)),
            None => Box::pin(async {}),
        }
    }
}

#[async_trait::async_trait]
impl Runnable for MatrixFetcher {
    type Error = Error;

    async fn run(mut self, mut control: ControlReceiver) -> Result<(), Self::Error> {
        info!(target: "matrix-fetcher", "Logging in...");
        let client = self.login_with_session().await?;
        info!(target: "matrix-fetcher", "...done!");
        let mut task: Pin<Box<dyn Future<Output = ()> + Send>> = Box::pin(async {});
        loop {
            tokio::select! {
                _ = &mut task => {
                    trace!(target: "matrix-fetcher", "Syncing...");
                    sync(&client).await;
                    trace!(target: "matrix-fetcher", "Updating known rooms...");
                    self.filter.update_rooms(client.rooms()).await;
                    trace!(target: "matrix-fetcher", "Generating notifications...");
                    self.generate_notifications(&client).await;
                    trace!(target: "matrix-fetcher", "Sleeping...");
                    task = self.sleep_task();
                },
                command = control.recv() => match command {
                    None => return Err(Error::InternalError("command stream closed unexpectedly".to_string())),
                    Some(command) => match command {
                        ControlMessage::Exit => break,
                        ControlMessage::Silence(duration) => task = Box::pin(sleep(duration)),
                        ControlMessage::GetRooms(result) => if result.send(self.filter.known_rooms()).is_err() {
                            warn!(target: "matrix-fetcher", "Failed to send get rooms response.")
                        },
                        ControlMessage::MuteRoom(local_room_id, result) => {
                            let response = self.filter.mute_room(local_room_id).map(|_| self.filter.known_rooms());
                            if result.send(response).is_err() {
                                warn!(target: "matrix-fetcher", "Failed to send mute room response.")
                            }
                        },
                        ControlMessage::UnmuteRoom(local_room_id, result) => {
                            let response = self.filter.unmute_room(local_room_id).map(|_| self.filter.known_rooms());
                            if result.send(response).is_err() {
                                warn!(target: "matrix-fetcher", "Failed to send unmute room response.")
                            }
                        },
                    },
                },
            }
        }
        info!(target: "matrix-fetcher", "Saving options...");
        self.options.muted_rooms = self.filter.muted_rooms();
        self.options.save(self.user_data_dir)?;
        info!(target: "matrix-fetcher", "...done.");
        info!(target: "matrix-fetcher", "Exitting.");
        Ok(())
    }
}

#[async_trait::async_trait]
impl Fetcher<Error> for MatrixFetcher {
    fn setup_args<'a>() -> ArgList<'a> {
        Self::common_args()
    }

    async fn setup(config: Config, options: &clap::ArgMatches) -> Result<(), Self::Error> {
        let Config { data_dir, .. } = config;
        let user_id: String = options.value_of_t_or_exit("username");
        let user_id = Box::try_from(user_id)?;
        let user_data_dir = user_data_dir(&user_id, data_dir);
        let options = match FetcherOptions::load(user_data_dir.clone()) {
            Ok(options) => options,
            Err(_) => Default::default(),
        }
        .updated_with(options)?;
        info!(target: "matrix-fetcher", "Starting setup...");
        Self::setup_session(user_id, user_data_dir.clone(), options.clone()).await?;
        info!(target: "matrix-fetcher", "Saving options...");
        options.save(user_data_dir)
    }

    fn run_args<'a>() -> ArgList<'a> {
        Self::common_args()
    }

    fn build(
        config: Config,
        options: &clap::ArgMatches,
        notifications_for_world: NotificationSender,
    ) -> Result<Self, Self::Error> {
        let Config { data_dir, .. } = config;
        let user_id: String = options.value_of_t_or_exit("username");
        let user_id = Box::try_from(user_id)?;
        let user_data_dir = user_data_dir(&user_id, data_dir);
        let options = match FetcherOptions::load(user_data_dir.clone()) {
            Ok(options) => options,
            Err(e) => {
                warn!(target: "matrix-fetcher", "Failed to load options, using empty defaults: {}", e);
                Default::default()
            },
        }.updated_with(options)?;
        Ok(Self {
            user_data_dir,
            user_id,
            filter: Filter::new(options.muted_rooms.clone()),
            options,
            notifications_for_world,
        })
    }
}
