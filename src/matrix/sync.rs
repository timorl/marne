use matrix_sdk::{config::SyncSettings, Client, LoopCtrl};
use std::sync::{
    atomic::{AtomicUsize, Ordering},
    Arc,
};

async fn sync_settings(client: &Client) -> SyncSettings<'_> {
    match client.sync_token().await {
        Some(sync_token) => SyncSettings::default().token(sync_token),
        None => SyncSettings::default(),
    }
}

pub async fn sync(client: &Client) {
    let sync_settings = sync_settings(client).await;
    let repetitions = &Arc::new(AtomicUsize::new(3));
    client
        .sync_with_callback(sync_settings, |_| async move {
            match repetitions.fetch_sub(1, Ordering::Relaxed) {
                0 => LoopCtrl::Break,
                _ => LoopCtrl::Continue,
            }
        })
        .await;
}
