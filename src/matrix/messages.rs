use log::warn;
use matrix_sdk::{
    room::{MessagesOptions, Room},
    ruma::{
        events::{
            room::message::{MessageType, RoomMessageEvent},
            AnyMessageEvent, AnyRoomEvent,
        },
        EventId,
    },
};

use crate::Notification;

async fn get_messages_from(room: &Room, sync_token: &str) -> (Vec<AnyRoomEvent>, Option<String>) {
    let request = MessagesOptions::backward(sync_token);
    let response = match room.messages(request).await {
        Ok(response) => response,
        Err(e) => {
            warn!(target: "matrix-fetcher", "Failed messages request: {}", e);
            return (Vec::new(), None);
        }
    };
    (
        response
            .chunk
            .iter()
            .filter_map(|event| match event.event.deserialize() {
                Ok(event) => Some(event),
                Err(e) => {
                    warn!(target: "matrix-fetcher", "Failed to deserialize event: {}", e);
                    None
                }
            })
            .collect(),
        response.end,
    )
}

pub async fn messages_until(
    room: &Room,
    mut sync_token: String,
    until: Option<Box<EventId>>,
) -> Vec<RoomMessageEvent> {
    let mut result = Vec::new();
    for _ in 0..100_usize {
        let (events, token) = get_messages_from(room, &sync_token).await;
        for event in events {
            if until.as_ref().map(|event_box| event_box.as_ref()) == Some(event.event_id()) {
                return result;
            }
            if let AnyRoomEvent::Message(AnyMessageEvent::RoomMessage(message)) = event {
                result.push(message);
            }
        }
        sync_token = match token {
            Some(token) => token,
            None => return result,
        };
    }
    result
}

fn format_single_message(message: RoomMessageEvent) -> String {
    let sender_id = message.sender;
    use MessageType::*;
    let content = match message.content.msgtype {
        Text(text) => text.body,
        Audio(_) => "𝅘𝅥𝅮 some noise 𝅘𝅥𝅮".into(),
        Emote(_) => "☺ some emote ☹".into(),
        File(_) => "🗃some file 📦".into(),
        Image(_) => "🖌some image 🎨".into(),
        Location(_) => "🧭 some location 🗺".into(),
        Notice(_) | ServerNotice(_) => "! some notice ¡".into(),
        Video(_) => "🎥 some video 🎞".into(),
        _ => "? unknown message type ?".into(),
    };
    format!("{}: {}", sender_id, content)
}

pub fn create_notification(name: String, messages: Vec<RoomMessageEvent>) -> Option<Notification> {
    let body = match messages.len() {
        0 => return None,
        1..=6 => {
            let mut result = Vec::new();
            for message in messages {
                result.push(format_single_message(message));
            }
            result
                .iter()
                .fold("".into(), |a, i| format!("{}\n{}", a, i))
        }
        n_messages => {
            format!("{} new messages in room.", n_messages)
        }
    };
    Some(Notification {
        summary: name,
        body,
        command: "".into(),
    })
}
