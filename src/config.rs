use std::path;

pub type ArgList<'help> = Vec<clap::Arg<'help>>;

pub struct Config {
    pub data_dir: path::PathBuf,
    pub runtime_dir: path::PathBuf,
}
