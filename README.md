# marne

**Ma**trix **R**ust **N**otifier **E**xtraordinaire (marne, pronounced マルネ) is a lightweight [Matrix](https://matrix.org/) client for producing notifications about outstanding unread messages written in Rust.
It is designed to be run on Linux mobile phones such as the [Pinephone](https://www.pine64.org/pinephone/), but can also be run on desktops.
It also supports [sxmo](https://sxmo.org/) notifications.

marne – the quality is in the name!

## Features

In no particular order.

- [x] Create notifications from unencrypted rooms.
- [x] Create notifications from encrypted rooms.
- [x] Deliver notifications using libnotify.
- [x] Deliver notifications using the sxmo setup.
- [x] Per-room notification filtering.
- [x] Silencing notifications for a given period of time.
- [ ] Launching a configurable client on notification click.
- [ ] Requesting a wait for a successful sync.
- [ ] Notifying on too many unsuccessful consecutive syncs.
- [ ] Mystery feature f5e90087e5e541ec9cdd625dd18a84d81f05ecbe08a12f1d18df8544c1893dd8.
- [ ] Mystery feature dc89018a091ddc196060b13f527040d2b0038d00f7d5a7896ed1a6d36aed40dc.

Abandoned:

- Running as a daemon. Probably better leave this to OS tools.

## Building

To build the version for sxmo run `cargo build --release`, to build the version for any other Linux system run
```sh
cargo build --features libnotify --no-default-features --release
```
(is my bias showing?).

There is probably some way to build for a specific architecture, but I'm too lazy to figure it out, so just build on your Pinephone or something.

You'll need some build dependencies on the phone, on postmarketOS the following should suffice:
```
apk add build-base openssl-dev patchelf
```

Oh, and it seems there is a bug somewhere between cargo/ld/musl/sxmo, where compiling on your phone results in a perfectly fine binary,
which thinks its interpreter path is `/lib/ld-linux-aarch64.so.1` while it clearly should be `/lib/ld-musl-aarch64.so.1`.
To make that work do
```
patchelf --set-interpreter /lib/ld-musl-aarch64.so.1 $THE_BINARY_YOU_WANT_TO_USE
```

## Usage

First you have to setup the connection, a minimal example would look like
```sh
marne setup -u @user:matrix.example.org
```
and then input your password when prompted.

After the setup is done, you can run
```sh
marne run -u @user:matrix.example.org
```
and it should produce notifications.

For more information use `marne help` for the specific commands.

## Licence

Everything in this repo that can be, is available under the GPL licence, v3 or later, as in the COPYING file.
